package com.example.cache.mapper;

import com.example.cache.model.CityEntity;
import com.example.cache.model.WeatherEntity;
import com.example.domain.model.City;
import com.example.domain.model.Weather;

public interface EntityMapper {
    Weather mapWeatherFromCached(WeatherEntity type);

    WeatherEntity mapWeatherToCached(Weather type);

    City mapCityFromCached(CityEntity type);

    CityEntity mapCityToCached(City type);
}
