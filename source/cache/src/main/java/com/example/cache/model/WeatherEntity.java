package com.example.cache.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.cache.db.Const;

@Entity(tableName = Const.WEATHER_TABLE_NAME)
public class WeatherEntity {
    @NonNull
    @PrimaryKey
    public long datetimeTimestamp;
    public long id;
    public String title;
    public String description;
    public String icon;
    public String datetimeString;
    public double temperature;
    public double minTemperature;
    public double maxTemperature;
    public double humidity;
    public double pressure;
}