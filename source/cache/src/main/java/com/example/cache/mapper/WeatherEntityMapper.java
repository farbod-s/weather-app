package com.example.cache.mapper;

import com.example.cache.model.CityEntity;
import com.example.cache.model.WeatherEntity;
import com.example.domain.model.City;
import com.example.domain.model.Weather;

import javax.inject.Inject;

public class WeatherEntityMapper implements EntityMapper {
    @Inject
    public WeatherEntityMapper() {
        // no-implementation
    }

    @Override
    public Weather mapWeatherFromCached(WeatherEntity type) {
        Weather weather = new Weather();
        weather.id = type.id;
        weather.title = type.title;
        weather.temperature = type.temperature;
        weather.pressure = type.pressure;
        weather.minTemperature = type.minTemperature;
        weather.maxTemperature = type.maxTemperature;
        weather.icon = type.icon;
        weather.humidity = type.humidity;
        weather.description = type.description;
        weather.datetimeTimestamp = type.datetimeTimestamp;
        weather.datetimeString = type.datetimeString;

        return weather;
    }

    @Override
    public WeatherEntity mapWeatherToCached(Weather type) {
        WeatherEntity weatherEntity = new WeatherEntity();
        weatherEntity.title = type.title;
        weatherEntity.temperature = type.temperature;
        weatherEntity.pressure = type.pressure;
        weatherEntity.minTemperature = type.minTemperature;
        weatherEntity.maxTemperature = type.maxTemperature;
        weatherEntity.icon = type.icon;
        weatherEntity.humidity = type.humidity;
        weatherEntity.description = type.description;
        weatherEntity.datetimeTimestamp = type.datetimeTimestamp;
        weatherEntity.datetimeString = type.datetimeString;

        return weatherEntity;
    }

    @Override
    public City mapCityFromCached(CityEntity type) {
        return new City(type.id,
                type.name,
                type.country,
                type.timezone);
    }

    @Override
    public CityEntity mapCityToCached(City type) {
        CityEntity cityEntity = new CityEntity();
        cityEntity.id = type.id;
        cityEntity.name = type.name;
        cityEntity.country = type.country;
        cityEntity.timezone = type.timezone;

        return cityEntity;
    }
}
