package com.example.cache.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.cache.db.Const;
import com.example.cache.model.CityEntity;

import java.util.List;

@Dao
public abstract class CityDao {
    @Query(Const.QUERY_CITIES)
    public abstract List<CityEntity> getCities();

    @Query(Const.DELETE_ALL_CITIES)
    public abstract void clearCities();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertCity(CityEntity cityEntity);
}
