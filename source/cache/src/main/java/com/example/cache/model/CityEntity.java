package com.example.cache.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.cache.db.Const;

@Entity(tableName = Const.CITY_TABLE_NAME)
public class CityEntity {
    @NonNull
    @PrimaryKey
    public long id;
    public String name;
    public String country;
    public int timezone;
}
