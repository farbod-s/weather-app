package com.example.cache;

import com.example.cache.db.AppDatabase;
import com.example.cache.mapper.WeatherEntityMapper;
import com.example.cache.model.CityEntity;
import com.example.cache.model.WeatherEntity;
import com.example.cache.utility.PreferencesHelper;
import com.example.data.repository.WeatherCache;
import com.example.domain.model.Location;
import com.example.domain.model.Weather;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class WeatherCacheImp implements WeatherCache {
    private static final long EXPIRATION_TIME = 30 * 60 * 1000; // 30 minutes

    private AppDatabase database;
    private WeatherEntityMapper weatherEntityMapper;
    private PreferencesHelper preferencesHelper;

    @Inject
    public WeatherCacheImp(AppDatabase database,
                           WeatherEntityMapper weatherEntityMapper,
                           PreferencesHelper preferencesHelper) {
        this.database = database;
        this.weatherEntityMapper = weatherEntityMapper;
        this.preferencesHelper = preferencesHelper;
    }

    @Override
    public Completable clearWeather() {
        return Completable.defer(() -> {
            database.getWeatherDao().clearWeathers();
            database.getCityDao().clearCities();
            return Completable.complete();
        });
    }

    @Override
    public Completable saveWeather(WeatherForecast weather) {
        return Completable.defer(() -> {
            for (Weather item : weather.weatherList) {
                WeatherEntity weatherEntity = weatherEntityMapper.mapWeatherToCached(item);
                database.getWeatherDao().insertWeather(weatherEntity);
            }
            CityEntity cityEntity = weatherEntityMapper.mapCityToCached(weather.city);
            database.getCityDao().insertCity(cityEntity);
            return Completable.complete();
        });
    }

    @Override
    public Single<WeatherForecast> getWeather(WeatherFilter filter) {
        return Single.defer(() -> Single.just(database.getCityDao().getCities())
                .zipWith(Single.just(database.getWeatherDao().getWeathers()),
                        (cityEntities, weatherEntities) -> {
                            List<Weather> list = new ArrayList<>();
                            for (WeatherEntity item : weatherEntities) {
                                Weather weather = weatherEntityMapper.mapWeatherFromCached(item);
                                list.add(weather);
                            }
                            WeatherForecast weatherForecast = new WeatherForecast();
                            weatherForecast.weatherList = new ArrayList<>(list);
                            if (cityEntities != null && cityEntities.size() > 0) {
                                CityEntity cityEntity = cityEntities.get(0);
                                weatherForecast.city = weatherEntityMapper.mapCityFromCached(cityEntity);
                            }
                            return weatherForecast;
                        })
        );
    }

    @Override
    public Single<Boolean> isCached(Location location) {
        if (location == null || location.isEmpty()) {
            // refuse cache when location is empty
            return Single.just(false);
        }
        return Single.defer(() -> Single
                .just(!database.getCityDao().getCities().isEmpty()));
    }

    @Override
    public void setLastCacheTime(long lastCache) {
        preferencesHelper.setLastCacheTime(lastCache);
    }

    @Override
    public void setLastLocation(Location location) {
        if (location != null) {
            preferencesHelper.setLastLocation(location.toString());
        }
    }

    @Override
    public Single<Boolean> isExpired(Location location) {
        // check time
        long currentTime = System.currentTimeMillis();
        long lastUpdateTime = preferencesHelper.getLastCacheTime();
        boolean isTimeExpired = currentTime - lastUpdateTime > EXPIRATION_TIME;

        return Single.defer(() -> Single.just(isTimeExpired));
    }
}
