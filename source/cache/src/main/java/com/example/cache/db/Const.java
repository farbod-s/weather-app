package com.example.cache.db;

public class Const {
    public static final String WEATHER_TABLE_NAME = "weathers";
    public static final String CITY_TABLE_NAME = "cities";

    public static final String QUERY_WEATHERS = "SELECT * FROM " + WEATHER_TABLE_NAME + " ORDER BY datetimeTimestamp ASC";
    public static final String QUERY_CITIES = "SELECT * FROM " + CITY_TABLE_NAME + "";

    public static final String DELETE_ALL_WEATHERS = "DELETE FROM " + WEATHER_TABLE_NAME;
    public static final String DELETE_ALL_CITIES = "DELETE FROM " + CITY_TABLE_NAME;
}
