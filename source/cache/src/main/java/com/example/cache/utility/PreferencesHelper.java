package com.example.cache.utility;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesHelper {
    private static final String PREF_PACKAGE_NAME = "com.example.weatherapp.preferences";
    private static final String PREF_KEY_LAST_CACHE = "last_cache";
    private static final String PREF_KEY_LAST_LOCATION = "last_location";

    private SharedPreferences preferences;

    @Inject
    public PreferencesHelper(Context context) {
        this.preferences = context.getSharedPreferences(PREF_PACKAGE_NAME, Context.MODE_PRIVATE);
    }

    public long getLastCacheTime() {
        return preferences.getLong(PREF_KEY_LAST_CACHE, 0);
    }

    public void setLastCacheTime(long lastCache) {
        preferences.edit().putLong(PREF_KEY_LAST_CACHE, lastCache).apply();
    }

    public String getLastLocation() {
        return preferences.getString(PREF_KEY_LAST_LOCATION, "");
    }

    public void setLastLocation(String location) {
        preferences.edit().putString(PREF_KEY_LAST_LOCATION, location).apply();
    }
}
