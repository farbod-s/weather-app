package com.example.cache.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.cache.db.Const;
import com.example.cache.model.WeatherEntity;

import java.util.List;

@Dao
public abstract class WeatherDao {
    @Query(Const.QUERY_WEATHERS)
    public abstract List<WeatherEntity> getWeathers();

    @Query(Const.DELETE_ALL_WEATHERS)
    public abstract void clearWeathers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertWeather(WeatherEntity weatherEntity);
}
