package com.example.cache.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.cache.dao.CityDao;
import com.example.cache.dao.WeatherDao;
import com.example.cache.model.CityEntity;
import com.example.cache.model.WeatherEntity;

@Database(entities = {WeatherEntity.class, CityEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "weather.db";
    private static final Object lock = new Object();
    private static AppDatabase INSTANCE = null;

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DATABASE_NAME)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract WeatherDao getWeatherDao();

    public abstract CityDao getCityDao();
}

