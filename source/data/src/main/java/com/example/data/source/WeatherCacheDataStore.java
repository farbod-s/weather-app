package com.example.data.source;

import com.example.data.repository.WeatherCache;
import com.example.data.repository.WeatherDataStore;
import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class WeatherCacheDataStore implements WeatherDataStore {
    private WeatherCache weatherCache;

    @Inject
    public WeatherCacheDataStore(WeatherCache weatherCache) {
        this.weatherCache = weatherCache;
    }

    @Override
    public Completable clearWeather() {
        return weatherCache.clearWeather();
    }

    @Override
    public Completable saveWeather(WeatherForecast weather, Location location) {
        if (location == null || location.isEmpty()) {
            // do not save when location is empty
            return Completable.complete();
        }
        return weatherCache.saveWeather(weather).doOnComplete(() -> {
            weatherCache.setLastCacheTime(System.currentTimeMillis());
            weatherCache.setLastLocation(location);
        });
    }

    @Override
    public Single<WeatherForecast> getWeather(WeatherFilter filter) {
        return weatherCache.getWeather(filter);
    }

    @Override
    public Flowable<List<WeatherForecast>> findWeather(WeatherFilter filter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<Boolean> isCached(Location location) {
        return weatherCache.isCached(location);
    }

    @Override
    public Single<Boolean> isExpired(Location location) {
        return weatherCache.isExpired(location);
    }
}