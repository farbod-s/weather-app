package com.example.data;

import com.example.data.source.WeatherDataStoreFactory;
import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;
import com.example.domain.repository.WeatherRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Singleton
public class WeatherDataRepository implements WeatherRepository {
    private WeatherDataStoreFactory weatherDataStoreFactory;

    @Inject
    public WeatherDataRepository(WeatherDataStoreFactory weatherDataStoreFactory) {
        this.weatherDataStoreFactory = weatherDataStoreFactory;
    }

    @Override
    public Completable clearWeather() {
        return weatherDataStoreFactory.retrieveCacheDataStore().clearWeather();
    }

    @Override
    public Completable saveWeather(WeatherForecast weather, Location location) {
        return weatherDataStoreFactory.retrieveCacheDataStore().saveWeather(weather, location);
    }

    @Override
    public Single<WeatherForecast> getWeather(WeatherFilter filter) {
        return Single.just(filter.location != null && !filter.location.isEmpty())
                .flatMap(checkCache -> {
                    if (checkCache) {
                        return weatherDataStoreFactory.retrieveCacheDataStore().isExpired(filter.location);
                    }
                    return Single.just(false);
                })
                .flatMap(isExpired -> {
                    if (isExpired) {
                        return this.clearWeather().toSingle(() -> true);
                    }
                    return Single.just(false);
                })
                .zipWith(weatherDataStoreFactory.retrieveCacheDataStore().isCached(filter.location),
                        (isExpired, isCached) -> isCached && !isExpired)
                .flatMap(useCache -> weatherDataStoreFactory.retrieveDataStore(useCache)
                        .getWeather(filter))
                .flatMap(weatherForecast ->
                        this.saveWeather(weatherForecast,
                                filter.location).toSingle(() -> weatherForecast));
    }

    @Override
    public Flowable<List<WeatherForecast>> findWeather(WeatherFilter filter) {
        return weatherDataStoreFactory.retrieveRemoteDataStore().findWeather(filter);
    }
}