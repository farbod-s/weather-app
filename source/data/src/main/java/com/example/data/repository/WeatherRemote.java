package com.example.data.repository;

import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface WeatherRemote {
    Single<WeatherForecast> getWeather(WeatherFilter filter);

    Flowable<List<WeatherForecast>> findWeather(WeatherFilter filter);
}
