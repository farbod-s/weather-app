package com.example.data.source;

import com.example.data.repository.WeatherDataStore;
import com.example.data.repository.WeatherRemote;
import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class WeatherRemoteDataStore implements WeatherDataStore {
    private WeatherRemote weatherRemote;

    @Inject
    public WeatherRemoteDataStore(WeatherRemote weatherRemote) {
        this.weatherRemote = weatherRemote;
    }

    @Override
    public Completable clearWeather() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Completable saveWeather(WeatherForecast weather, Location location) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<WeatherForecast> getWeather(WeatherFilter filter) {
        return weatherRemote.getWeather(filter);
    }

    @Override
    public Flowable<List<WeatherForecast>> findWeather(WeatherFilter filter) {
        return weatherRemote.findWeather(filter);
    }

    @Override
    public Single<Boolean> isCached(Location location) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<Boolean> isExpired(Location location) {
        throw new UnsupportedOperationException();
    }
}