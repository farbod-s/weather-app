package com.example.data.executor;

import java.util.concurrent.ThreadFactory;

class JobThreadFactory implements ThreadFactory {
    private static final String THREAD_NAME = "android_";
    private int counter = 0;

    @Override
    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, THREAD_NAME + (counter++));
    }
}
