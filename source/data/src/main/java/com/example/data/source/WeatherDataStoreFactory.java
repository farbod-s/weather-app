package com.example.data.source;

import com.example.data.repository.WeatherDataStore;

import javax.inject.Inject;

public class WeatherDataStoreFactory {
    private WeatherCacheDataStore weatherCacheDataStore;
    private WeatherRemoteDataStore weatherRemoteDataStore;

    @Inject
    public WeatherDataStoreFactory(WeatherCacheDataStore weatherCacheDataStore,
                                   WeatherRemoteDataStore weatherRemoteDataStore) {
        this.weatherCacheDataStore = weatherCacheDataStore;
        this.weatherRemoteDataStore = weatherRemoteDataStore;
    }

    public WeatherDataStore retrieveDataStore(boolean useCache) {
        if (useCache) {
            return retrieveCacheDataStore();
        }
        return retrieveRemoteDataStore();
    }

    public WeatherDataStore retrieveCacheDataStore() {
        return weatherCacheDataStore;
    }

    public WeatherDataStore retrieveRemoteDataStore() {
        return weatherRemoteDataStore;
    }
}