package com.example.data.repository;

import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface WeatherCache {
    Completable clearWeather();

    Completable saveWeather(WeatherForecast weather);

    Single<WeatherForecast> getWeather(WeatherFilter filter);

    Single<Boolean> isCached(Location location);

    void setLastCacheTime(long lastCache);

    void setLastLocation(Location location);

    Single<Boolean> isExpired(Location location);
}
