package com.example.presentation.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;
import com.example.domain.usecase.GetWeather;
import com.example.presentation.data.Resource;
import com.example.presentation.data.ResourceState;
import com.example.presentation.mapper.WeatherViewMapper;
import com.example.presentation.model.CurrentWeather;
import com.example.presentation.model.WeatherItem;
import com.example.presentation.model.WeatherView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.subscribers.DisposableSubscriber;

public class GetWeatherViewModel extends ViewModel {
    private MutableLiveData<Resource<List<WeatherItem>>> weatherLiveData;
    private GetWeather getWeather;
    private WeatherViewMapper weatherViewMapper;

    @Inject
    public GetWeatherViewModel(GetWeather getWeather,
                               WeatherViewMapper weatherViewMapper) {
        super();

        this.getWeather = getWeather;
        this.weatherViewMapper = weatherViewMapper;
        this.weatherLiveData = new MutableLiveData<>();
    }

    @Override
    protected void onCleared() {
        getWeather.dispose();

        super.onCleared();
    }

    public LiveData<Resource<List<WeatherItem>>> getWeather() {
        return weatherLiveData;
    }

    public void fetchWeatherByLocation(Location location) {
        weatherLiveData.postValue(new Resource<>(ResourceState.LOADING, null, null));
        if (location == null || location.isEmpty()) {
            weatherLiveData.postValue(new Resource<>(ResourceState.SUCCESS, null, null));
            return;
        }
        getWeather.execute(new WeatherSubscriber(), new WeatherFilter(location, null));
    }

    public void fetchWeatherByName(String cityName) {
        weatherLiveData.postValue(new Resource<>(ResourceState.LOADING, null, null));
        if (cityName == null || cityName.isEmpty()) {
            weatherLiveData.postValue(new Resource<>(ResourceState.SUCCESS, null, null));
            return;
        }
        getWeather.execute(new WeatherSubscriber(), new WeatherFilter(null, cityName));
    }

    private class WeatherSubscriber extends DisposableSubscriber<WeatherForecast> {
        @Override
        public void onNext(WeatherForecast weatherForecast) {
            WeatherView weatherView = weatherViewMapper.mapToView(weatherForecast);
            List<WeatherItem> list = new ArrayList<>();

            // current weather
            CurrentWeather currentWeather = weatherView.getCurrentWeather();
            currentWeather.cityId = weatherView.cityId;
            currentWeather.cityName = weatherView.cityName;
            currentWeather.countryName = weatherView.countryName;

            WeatherItem item1 = new WeatherItem();
            item1.type = 2;
            item1.currentWeather = currentWeather;

            // today header
            WeatherItem item2 = new WeatherItem();
            item2.type = 1;
            item2.headerName = "Today";

            // today weather list
            List<CurrentWeather> weatherTodayList = new ArrayList<>();
            for (Map.Entry<String, CurrentWeather> entry : weatherView.todayWeatherList.entrySet()) {
                CurrentWeather weather = entry.getValue();
                weather.weatherTitle = entry.getKey();
                weatherTodayList.add(weather);
            }
            WeatherItem item3 = new WeatherItem();
            item3.type = 3;
            item3.weatherList = weatherTodayList;

            // next days header
            WeatherItem item4 = new WeatherItem();
            item4.type = 1;
            item4.headerName = "Next 5 days";

            // next days weather list
            List<CurrentWeather> weatherNextDaysList = new ArrayList<>();
            for (Map.Entry<String, List<CurrentWeather>> entry : weatherView.nextDaysWeatherList.entrySet()) {
                CurrentWeather weather = entry.getValue().get(entry.getValue().size() > 4 ? 4 : 0);
                weather.weatherTitle = entry.getKey();
                weatherNextDaysList.add(weather);
            }

            WeatherItem item5 = new WeatherItem();
            item5.type = 3;
            item5.weatherList = weatherNextDaysList;

            list.add(item1);
            list.add(item2);
            list.add(item3);
            list.add(item4);
            list.add(item5);

            weatherLiveData.postValue(new Resource<>(ResourceState.SUCCESS, list, null));
        }

        @Override
        public void onError(Throwable t) {
            weatherLiveData.postValue(new Resource<>(ResourceState.ERROR, null, t.getMessage()));
        }

        @Override
        public void onComplete() {
            // no-implementation
        }
    }
}
