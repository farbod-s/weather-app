package com.example.presentation.data;

public enum ResourceState {
    SUCCESS, ERROR, LOADING
}