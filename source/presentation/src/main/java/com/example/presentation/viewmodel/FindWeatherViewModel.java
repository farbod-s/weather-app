package com.example.presentation.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;
import com.example.domain.usecase.FindWeather;
import com.example.presentation.data.Resource;
import com.example.presentation.data.ResourceState;
import com.example.presentation.mapper.WeatherViewMapper;
import com.example.presentation.model.WeatherView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.subscribers.DisposableSubscriber;

public class FindWeatherViewModel extends ViewModel {
    private MutableLiveData<Resource<List<WeatherView>>> weatherLiveData;
    private FindWeather findWeather;
    private WeatherViewMapper weatherViewMapper;

    @Inject
    public FindWeatherViewModel(FindWeather findWeather,
                                WeatherViewMapper weatherViewMapper) {
        super();

        this.findWeather = findWeather;
        this.weatherViewMapper = weatherViewMapper;
        this.weatherLiveData = new MutableLiveData<>();
    }

    @Override
    protected void onCleared() {
        findWeather.dispose();

        super.onCleared();
    }

    public LiveData<Resource<List<WeatherView>>> getWeatherList() {
        return weatherLiveData;
    }

    public void fetchWeatherList(String cityName) {
        weatherLiveData.postValue(new Resource<>(ResourceState.LOADING, null, null));
        if (cityName == null || cityName.isEmpty()) {
            weatherLiveData.postValue(new Resource<>(ResourceState.SUCCESS, null, null));
            return;
        }
        findWeather.execute(new WeatherListSubscriber(), new WeatherFilter(null, cityName));
    }

    private class WeatherListSubscriber extends DisposableSubscriber<List<WeatherForecast>> {
        @Override
        public void onNext(List<WeatherForecast> weatherForecast) {
            List<WeatherView> list = new ArrayList<>();
            for (WeatherForecast forecast : weatherForecast) {
                WeatherView weatherView = weatherViewMapper.mapToView(forecast);
                list.add(weatherView);
            }
            weatherLiveData.postValue(new Resource<>(ResourceState.SUCCESS, list, null));
        }

        @Override
        public void onError(Throwable t) {
            weatherLiveData.postValue(new Resource<>(ResourceState.ERROR, null, t.getMessage()));
        }

        @Override
        public void onComplete() {
            // no-implementation
        }
    }
}
