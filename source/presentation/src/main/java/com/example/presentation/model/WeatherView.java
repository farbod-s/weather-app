package com.example.presentation.model;

import java.util.List;
import java.util.SortedMap;

public final class WeatherView {
    public long cityId;
    public String cityName;
    public String countryName;
    public SortedMap<String, CurrentWeather> todayWeatherList;
    public SortedMap<String, List<CurrentWeather>> nextDaysWeatherList;

    public CurrentWeather getCurrentWeather() {
        return todayWeatherList.get(todayWeatherList.firstKey());
    }
}
