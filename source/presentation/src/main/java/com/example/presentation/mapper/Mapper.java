package com.example.presentation.mapper;

public interface Mapper<View, Model> {
    View mapToView(Model type);
}