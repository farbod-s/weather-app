package com.example.presentation.data;

import androidx.annotation.Nullable;

public class Resource<T> {
    public ResourceState status;
    public T data;
    public String message;

    public Resource(ResourceState status,
                    @Nullable T data,
                    @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public Resource<T> success(T data) {
        return new Resource<>(ResourceState.SUCCESS, data, null);
    }

    Resource<T> error(T data, String message) {
        return new Resource<>(ResourceState.ERROR, null, message);
    }

    Resource<T> loading() {
        return new Resource<>(ResourceState.LOADING, null, null);
    }
}
