package com.example.presentation.model;

public final class CurrentWeather {
    public long cityId;
    public String cityName;
    public String countryName;

    public String weatherTitle;
    public String weatherDescription;
    public String weatherIcon;
    public String datetimeString;
    public long datetimeTimestamp;
    public double temperature;
    public double minTemperature;
    public double maxTemperature;
    public double humidity;
    public double pressure;
}
