package com.example.presentation.mapper;

import com.example.domain.model.Weather;
import com.example.domain.model.WeatherForecast;
import com.example.presentation.model.CurrentWeather;
import com.example.presentation.model.WeatherView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TreeMap;

import javax.inject.Inject;

public class WeatherViewMapper implements Mapper<WeatherView, WeatherForecast> {
    private static SimpleDateFormat defaultFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private static SimpleDateFormat hourFormatter = new SimpleDateFormat("H", Locale.getDefault());
    private static SimpleDateFormat dayFormatter = new SimpleDateFormat("M/d", Locale.getDefault());
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    @Inject
    public WeatherViewMapper() {
        // no-implementation
    }

    @Override
    public WeatherView mapToView(WeatherForecast type) {
        WeatherView weatherView = new WeatherView();
        weatherView.cityId = type.city != null ? type.city.id : -1;
        weatherView.cityName = type.city != null ? type.city.name : "-";
        weatherView.countryName = type.city != null ? type.city.country : "-";
        weatherView.todayWeatherList = new TreeMap<>();
        weatherView.nextDaysWeatherList = new TreeMap<>();

        long currentTimestamp = System.currentTimeMillis();
        List<Weather> weathers = type.weatherList;
        for (Weather weather : weathers) {
            CurrentWeather currentWeather = new CurrentWeather();
            currentWeather.datetimeString = weather.datetimeString;
            currentWeather.datetimeTimestamp = weather.datetimeTimestamp;
            currentWeather.humidity = weather.humidity;
            currentWeather.maxTemperature = weather.maxTemperature;
            currentWeather.minTemperature = weather.minTemperature;
            currentWeather.pressure = weather.pressure;
            currentWeather.temperature = weather.temperature;
            currentWeather.weatherDescription = weather.description;
            currentWeather.weatherIcon = weather.icon;
            currentWeather.weatherTitle = weather.title;

            if (isSameDay(currentTimestamp,
                    weather.datetimeString)) {
                // today
                String hourKey = getFormattedHour(weather.datetimeString);
                weatherView.todayWeatherList.put(String.format("%sh", hourKey), currentWeather);
            } else {
                // next days
                String dateKey = getFormattedDay(weather.datetimeString);

                List<CurrentWeather> currentWeatherList = weatherView.nextDaysWeatherList.get(dateKey);
                if (currentWeatherList == null) {
                    currentWeatherList = new ArrayList<>();
                    currentWeatherList.add(currentWeather);
                    weatherView.nextDaysWeatherList.put(dateKey, currentWeatherList);
                } else if (!currentWeatherList.contains(currentWeather)) {
                    currentWeatherList.add(currentWeather);
                }
            }
        }

        return weatherView;
    }

    private static String getFormattedHour(String timestamp) {
        try {
            return hourFormatter.format(defaultFormatter.parse(timestamp));
        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }
        return "0";
    }

    private static String getFormattedDay(String timestamp) {
        try {
            return dayFormatter.format(defaultFormatter.parse(timestamp));
        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }
        return "0/0";
    }

    private static boolean isSameDay(long firstTimestamp,
                                     long secondTimestamp,
                                     int timezone,
                                     String country) {
        dateFormatter.setTimeZone(new SimpleTimeZone(timezone, country));
        return dateFormatter.format(new Date(firstTimestamp))
                .equals(dateFormatter.format(new Date(secondTimestamp * 1000)));
    }

    private static boolean isSameDay(long firstTimestamp,
                                     String secondTimestampString) {
        try {
            Date secondDate = defaultFormatter.parse(secondTimestampString);
            Date firstDate = new Date(firstTimestamp);
            return dateFormatter.format(firstDate)
                    .equals(dateFormatter.format(secondDate)) ||
                    firstDate.after(secondDate) /* fix [openweathermap.org] midnight issue */;
        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }
}
