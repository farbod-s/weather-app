package com.example.presentation.model;

import java.util.List;

public class WeatherItem {
    public int type; // { 1:header, 2:current, 3:weatherList }
    public String headerName;
    public CurrentWeather currentWeather;
    public List<CurrentWeather> weatherList;
}
