package com.example.weatherapp.ui.widget.actionbar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class ActionBarView extends FrameLayout {

    public ActionBarView(Context context) {
        super(context);
        init();
    }

    public ActionBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ActionBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setPadding(0, 0, 0, 0);
    }

    public void setCustomView(View view, LayoutParams layoutParams) {
        clearItems();
        addView(view, layoutParams);
    }

    public void clearItems() {
        removeAllViews();
    }
}