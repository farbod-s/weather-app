package com.example.weatherapp.ui.widget.search;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.jakewharton.rxbinding3.InitialValueObservable;

import io.reactivex.Observer;
import io.reactivex.android.MainThreadDisposable;

public class EditableTextViewTextObservable extends InitialValueObservable<CharSequence> {
    private final TextView view;

    EditableTextViewTextObservable(TextView view) {
        this.view = view;
    }

    @Override
    protected void subscribeListener(Observer<? super CharSequence> observer) {
        EditableTextViewTextObservable.Listener listener = new EditableTextViewTextObservable.Listener(view, observer);
        observer.onSubscribe(listener);
        view.addTextChangedListener(listener);
        view.setOnEditorActionListener(listener);
    }

    @Override
    protected CharSequence getInitialValue() {
        return view.getText();
    }

    final static class Listener extends MainThreadDisposable implements TextWatcher, TextView.OnEditorActionListener {
        private final TextView view;
        private final Observer<? super CharSequence> observer;

        Listener(TextView view, Observer<? super CharSequence> observer) {
            this.view = view;
            this.observer = observer;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // nothing!
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // nothing!
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!isDisposed()) {
                view.removeTextChangedListener(this);
                observer.onNext(s);
                view.addTextChangedListener(this);
            }
        }

        @Override
        protected void onDispose() {
            view.removeTextChangedListener(this);
        }

        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
            if (!isDisposed()) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    observer.onNext(textView.getText());
                    return true;
                }
            }
            return false;
        }
    }
}