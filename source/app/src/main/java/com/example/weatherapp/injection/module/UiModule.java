package com.example.weatherapp.injection.module;

import android.content.Context;
import android.location.LocationManager;

import com.example.domain.executor.PostExecutionThread;
import com.example.weatherapp.executor.UiThread;
import com.example.weatherapp.ui.MainActivity;
import com.example.weatherapp.ui.fragment.MainFragment;
import com.example.weatherapp.ui.fragment.SearchFragment;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class UiModule {

    @Binds
    public abstract PostExecutionThread bindPostExecutionThread(UiThread uiThread);

    @ContributesAndroidInjector
    public abstract MainActivity provideMainActivity();

    @ContributesAndroidInjector
    public abstract MainFragment provideMainFragment();

    @ContributesAndroidInjector
    public abstract SearchFragment provideSearchFragment();

    @Provides
    public static FusedLocationProviderClient provideLocationProvider(Context context) {
        return LocationServices.getFusedLocationProviderClient(context);
    }

    @Provides
    public static LocationManager provideLocationManager(Context context) {
        return (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }
}
