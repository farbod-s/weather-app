package com.example.weatherapp.ui.listener;

public interface ItemClickListener<Item> {
    void onItemClicked(Item item);
}
