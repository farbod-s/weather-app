package com.example.weatherapp.location;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

import java.lang.ref.WeakReference;

public class LocationCallbackReference extends LocationCallback {
    private WeakReference<LocationCallback> locationCallbackRef;

    LocationCallbackReference(LocationCallback locationCallback) {
        locationCallbackRef = new WeakReference<>(locationCallback);
    }

    @Override
    public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);

        if (locationCallbackRef != null && locationCallbackRef.get() != null) {
            locationCallbackRef.get().onLocationResult(locationResult);
        }
    }
}