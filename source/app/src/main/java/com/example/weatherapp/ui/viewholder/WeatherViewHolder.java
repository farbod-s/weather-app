package com.example.weatherapp.ui.viewholder;

import com.example.weatherapp.databinding.ItemWeatherBinding;

public class WeatherViewHolder extends BaseViewHolder {
    public ItemWeatherBinding binding;

    public WeatherViewHolder(ItemWeatherBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}