package com.example.weatherapp.ui.viewholder;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.presentation.model.CurrentWeather;
import com.example.weatherapp.databinding.ItemListBinding;
import com.example.weatherapp.ui.adapter.ForecastAdapter;

import java.util.List;

public class ForecastListViewHolder extends BaseViewHolder {
    public ItemListBinding binding;

    public ForecastListViewHolder(ItemListBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setupListView(List<CurrentWeather> weatherList) {
        ForecastAdapter adapter = new ForecastAdapter(weatherList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                binding.getRoot().getContext(),
                RecyclerView.HORIZONTAL,
                false
        );
        binding.recyclerBrowse.setLayoutManager(layoutManager);
        binding.recyclerBrowse.setAdapter(adapter);
    }
}