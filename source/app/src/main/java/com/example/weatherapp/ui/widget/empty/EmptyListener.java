package com.example.weatherapp.ui.widget.empty;

public interface EmptyListener {
    void onCheckAgainClicked();
}