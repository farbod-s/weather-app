package com.example.weatherapp.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.cache.utility.PreferencesHelper;
import com.example.domain.model.Location;
import com.example.presentation.model.WeatherItem;
import com.example.presentation.viewmodel.GetWeatherViewModel;
import com.example.weatherapp.databinding.ItemHeaderBinding;
import com.example.weatherapp.databinding.ItemListBinding;
import com.example.weatherapp.databinding.ItemWeatherBinding;
import com.example.weatherapp.databinding.ViewToolbarMainBinding;
import com.example.weatherapp.location.LocationListener;
import com.example.weatherapp.location.MathUtils;
import com.example.weatherapp.ui.listener.EndlessDataProvider;
import com.example.weatherapp.ui.navigation.Navigator;
import com.example.weatherapp.ui.viewholder.BaseViewHolder;
import com.example.weatherapp.ui.viewholder.ForecastListViewHolder;
import com.example.weatherapp.ui.viewholder.HeaderViewHolder;
import com.example.weatherapp.ui.viewholder.WeatherViewHolder;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class MainFragment extends EndlessFragment<WeatherItem, BaseViewHolder> implements LocationListener {
    @Inject
    public PreferencesHelper preferences;
    private GetWeatherViewModel viewModel;
    private Location location;
    private String cityName;
    private boolean firstRun = true;

    private EndlessDataProvider<WeatherItem> dataProvider = new EndlessDataProvider<WeatherItem>() {
        @Override
        public List<WeatherItem> getMoreData() {
            return null;
        }

        @Override
        public void getMoreDataAsync() {
            fetchWeather();
        }

        @Override
        public void reset() {
            // nothing!
        }
    };

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = getViewModel(GetWeatherViewModel.class);

        activity.setLocationListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (firstRun) {
            location = Location.fromString(preferences.getLastLocation());
            if (location == null || location.isEmpty()) {
                activity.requestLastLocation();
            }

            observeWeather();
            firstRun = false;
        }
    }

    @Override
    protected void deserializeArguments() {
        super.deserializeArguments();

        Bundle arguments = getArguments();
        if (arguments != null) {
            location = new Location(arguments.getString("latitude"),
                    arguments.getString("longitude"));
            cityName = arguments.getString("cityName");
        }
    }

    @Override
    public String getName() {
        return "MainFragment";
    }

    @Override
    protected View getActionBarView() {
        ViewToolbarMainBinding binding = ViewToolbarMainBinding.inflate(LayoutInflater.from(getContext()));
        binding.searchButton.setOnClickListener(searchView ->
                Navigator.addFragmentOnTop(activity.getSupportFragmentManager(),
                        SearchFragment.newInstance()));

        return binding.getRoot();
    }

    private void fetchWeather() {
        // check city name first, so we can handle search result
        if (cityName != null && !cityName.isEmpty()) {
            viewModel.fetchWeatherByName(cityName);
        } else {
            viewModel.fetchWeatherByLocation(location);
        }
    }

    private void observeWeather() {
        viewModel.getWeather().observe(this, this::appendData);
    }

    @Override
    protected BaseViewHolder onCreateItemHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1: { // Header
                ItemHeaderBinding binding =
                        ItemHeaderBinding.inflate(
                                LayoutInflater.from(parent.getContext()),
                                parent,
                                false
                        );
                return new HeaderViewHolder(binding);
            }

            case 2: { // current weather
                ItemWeatherBinding binding =
                        ItemWeatherBinding.inflate(
                                LayoutInflater.from(parent.getContext()),
                                parent,
                                false
                        );
                return new WeatherViewHolder(binding);
            }

            case 3: { // weather list
                ItemListBinding binding =
                        ItemListBinding.inflate(
                                LayoutInflater.from(parent.getContext()),
                                parent,
                                false
                        );
                return new ForecastListViewHolder(binding);
            }
        }

        return null;
    }

    @Override
    protected void onBindItemHolder(BaseViewHolder holder, WeatherItem weatherItem) {
        switch (weatherItem.type) {
            case 1: { // Header
                ((HeaderViewHolder) holder).binding.setHeaderName(weatherItem.headerName);
                break;
            }

            case 2: { // current weather
                ((WeatherViewHolder) holder).binding.setWeather(weatherItem.currentWeather);
                break;
            }

            case 3: { // weather list
                ((ForecastListViewHolder) holder).setupListView(weatherItem.weatherList);
                break;
            }
        }
    }

    @Override
    protected int getItemViewType(WeatherItem weatherItem) {
        return weatherItem.type;
    }

    @Override
    protected EndlessDataProvider<WeatherItem> getDataProvider() {
        return dataProvider;
    }

    @Override
    protected void onItemClicked(WeatherItem weatherItem) {
        // nothing!
    }

    @Override
    public void onLastLocationReceived(android.location.Location location) {
        // check distance
        String[] lastLatLong = this.location != null ? this.location.split() : new String[]{};
        String[] latLng = getLocationString(location).split(",");

        double lat1 = 0;
        double lng1 = 0;

        if (lastLatLong.length == 2) {
            try {
                lat1 = Double.parseDouble(lastLatLong[0]);
                lng1 = Double.parseDouble(lastLatLong[1]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        double lat2 = 0;
        double lng2 = 0;
        if (latLng.length == 2) {
            try {
                lat2 = Double.parseDouble(latLng[0]);
                lng2 = Double.parseDouble(latLng[1]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        boolean isLocationExpired = MathUtils.calculateDistance(lat1, lng1, lat2, lng2) > 1000; // 1km

        if (this.location == null) {
            this.location = new Location(latLng[0], latLng[1]);
        } else {
            this.location.latitude = latLng[0];
            this.location.longitude = latLng[1];
        }

        if (isLocationExpired) {
            resetList();
        }
    }

    private String getLocationString(android.location.Location location) {
        if (location == null) {
            return "";
        }

        return String.format(Locale.getDefault(),
                "%.7f,%.7f",
                location.getLatitude(),
                location.getLongitude());
    }
}
