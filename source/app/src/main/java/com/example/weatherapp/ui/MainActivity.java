package com.example.weatherapp.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.weatherapp.R;
import com.example.weatherapp.location.LocationHelper;
import com.example.weatherapp.location.LocationListener;
import com.example.weatherapp.location.LocationRequestListener;
import com.example.weatherapp.ui.fragment.MainFragment;
import com.example.weatherapp.ui.navigation.Navigator;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

import static com.example.weatherapp.location.LocationHelper.MY_PERMISSIONS_REQUEST_LOCATION;
import static com.example.weatherapp.location.LocationHelper.MY_REQUEST_LOCATION_ENABLE;

public class MainActivity extends AppCompatActivity implements LocationRequestListener {
    @Inject
    public LocationHelper locationHelper;

    private Snackbar snackBarPermission;
    private Snackbar snackBarLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);

        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_main);

        if (savedInstanceState == null) {
            showMainFragment();
        }

        locationHelper.setLocationRequestListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (locationHelper.isLocationPermissionGranted(this) &&
                locationHelper.isLocationEnabled(this)) {
            locationHelper.startLocationUpdates();
        } else {
            if (!locationHelper.isLocationPermissionGranted(this)) {
                requestLocationPermission();
            } else if (!locationHelper.isLocationEnabled(this)) {
                requestLocationEnable();
            }
        }
    }

    @Override
    protected void onStop() {
        locationHelper.stopLocationUpdates();

        super.onStop();
    }

    private void showMainFragment() {
        MainFragment mainFragment = MainFragment.newInstance();
        Navigator.replaceFragment(getSupportFragmentManager(), mainFragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_LOCATION_ENABLE) {
            if (resultCode == RESULT_OK) {
                onRequestLocationResult(true);
            }
        } else {
            onRequestLocationResult(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            // if request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (locationHelper.isLocationPermissionGranted(this)) {
                    // request location updates
                    if (locationHelper.isLocationEnabled(this)) {
                        locationHelper.startLocationUpdates();
                    } else {
                        requestLocationEnable();
                    }
                } else {
                    requestLocationPermission();
                }
            } else {
                requestLocationPermission();
            }
        }
    }

    @Override
    public void onRequestLocationResult(boolean locationEnabled) {
        if (locationEnabled) {
            locationHelper.startLocationUpdates();
        } else {
            requestLocationEnable();
        }
    }

    public void setLocationListener(LocationListener locationListener) {
        if (locationHelper != null) {
            locationHelper.setLocationListener(locationListener);
        }
    }

    public void requestLastLocation() {
        locationHelper.requestLastLocation();
    }

    private void requestLocationPermission() {
        if (snackBarPermission == null) {
            snackBarPermission = Snackbar.make(findViewById(R.id.container),
                    R.string.label_permission_description,
                    Snackbar.LENGTH_INDEFINITE);
            snackBarPermission.setAction(R.string.ok, view ->
                    locationHelper.requestLocationPermission(MainActivity.this));
        }
        snackBarPermission.show();
    }

    private void requestLocationEnable() {
        if (snackBarLocation == null) {
            snackBarLocation = Snackbar.make(findViewById(R.id.container),
                    R.string.label_location_description,
                    Snackbar.LENGTH_INDEFINITE);
            snackBarLocation.setAction(R.string.enable, view ->
                    locationHelper.enableLocation(MainActivity.this));
        }
        snackBarLocation.show();
    }
}
