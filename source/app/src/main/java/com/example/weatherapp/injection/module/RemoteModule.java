package com.example.weatherapp.injection.module;

import com.example.data.repository.WeatherRemote;
import com.example.remote.WeatherRemoteImp;
import com.example.remote.service.WeatherService;
import com.example.remote.service.WeatherServiceFactory;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class RemoteModule {

    @Provides
    public static WeatherService provideRemoteService() {
        return WeatherServiceFactory.makeWeatherService(true /*BuildConfig.DEBUG*/);
    }

    @Binds
    public abstract WeatherRemote bindWeatherRemote(WeatherRemoteImp remoteImp);
}
