package com.example.weatherapp.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.presentation.model.WeatherView;
import com.example.presentation.viewmodel.FindWeatherViewModel;
import com.example.weatherapp.MyApplication;
import com.example.weatherapp.databinding.ItemCityBinding;
import com.example.weatherapp.databinding.ViewToolbarSearchBinding;
import com.example.weatherapp.ui.listener.EndlessDataProvider;
import com.example.weatherapp.ui.navigation.Navigator;
import com.example.weatherapp.ui.widget.search.EditableRxTextView;
import com.example.weatherapp.ui.utility.AndroidUtils;
import com.example.weatherapp.ui.viewholder.CityViewHolder;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SearchFragment extends EndlessFragment<WeatherView, CityViewHolder> {
    private FindWeatherViewModel viewModel;
    private boolean firstRun = true;
    private String query;

    private EndlessDataProvider<WeatherView> dataProvider = new EndlessDataProvider<WeatherView>() {
        @Override
        public List<WeatherView> getMoreData() {
            return null;
        }

        @Override
        public void getMoreDataAsync() {
            fetchWeatherList();
        }

        @Override
        public void reset() {
            // nothing!
        }
    };

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = getViewModel(FindWeatherViewModel.class);
    }

    @Override
    protected CityViewHolder onCreateItemHolder(ViewGroup parent, int viewType) {
        ItemCityBinding binding =
                ItemCityBinding.inflate(
                        LayoutInflater.from(parent.getContext()),
                        parent,
                        false
                );
        binding.setListener(getClickListener());

        return new CityViewHolder(binding);
    }

    @Override
    protected void onBindItemHolder(CityViewHolder holder, WeatherView weatherView) {
        holder.binding.setWeather(weatherView);
        holder.binding.executePendingBindings();
    }

    @Override
    protected EndlessDataProvider<WeatherView> getDataProvider() {
        return dataProvider;
    }

    @Override
    protected void onItemClicked(WeatherView weatherView) {
        Bundle args = new Bundle();
        args.putString("cityName", weatherView.cityName);

        MainFragment mainFragment = MainFragment.newInstance();
        mainFragment.setArguments(args);

        Navigator.replaceFragment(activity.getSupportFragmentManager(), mainFragment);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (firstRun) {
            observeSearchResult();
            firstRun = false;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void fetchWeatherList() {
        viewModel.fetchWeatherList(query);
    }

    private void observeSearchResult() {
        viewModel.getWeatherList().observe(this, this::appendData);
    }

    @Override
    public String getName() {
        return "SearchFragment";
    }

    @SuppressLint("CheckResult")
    @Override
    protected View getActionBarView() {
        ViewToolbarSearchBinding binding = ViewToolbarSearchBinding.inflate(LayoutInflater.from(getContext()));
        binding.backButton.setOnClickListener(searchView -> activity.onBackPressed());
        binding.clearButton.setOnClickListener(clearView -> binding.searchText.getText().clear());
        binding.searchText.requestFocus();

        // show keyboard
        MyApplication.runOnUIThread(() -> AndroidUtils.showKeyboard(binding.searchText));

        // observe on search view changes
        EditableRxTextView.textChanges(binding.searchText)
                .map(CharSequence::toString)
                .flatMap(query -> {
                    binding.clearButton.setVisibility(query.length() > 0 ? View.VISIBLE : View.GONE);
                    return Observable.just(query);
                })
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter(query -> query.length() > 3)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::search);

        return binding.getRoot();
    }

    private void search(String query) {
        this.query = query;
        resetList();
    }
}
