package com.example.weatherapp.ui.viewholder;

import com.example.weatherapp.databinding.ItemHeaderBinding;

public class HeaderViewHolder extends BaseViewHolder {
    public ItemHeaderBinding binding;

    public HeaderViewHolder(ItemHeaderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}