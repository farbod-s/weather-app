package com.example.weatherapp.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;

import javax.inject.Inject;

public class LocationHelper {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final int MY_REQUEST_LOCATION_ENABLE = 98;

    private FusedLocationProviderClient fusedLocationClient;
    private LocationManager locationManager;

    private LocationRequest locationRequest;
    private LocationRequestListener locationRequestListener;
    private LocationListener locationListener;
    private LocationCallback locationCallback;
    private LocationCallbackReference locationCallbackReference;

    @Inject
    public LocationHelper(LocationManager locationManager,
                          FusedLocationProviderClient fusedLocationClient) {
        this.locationManager = locationManager;
        this.fusedLocationClient = fusedLocationClient;

        init();
    }

    private void init() {
        createLocationRequest();
        setupLocationCallback();
    }

    private void setupLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null && locationListener != null) {
                    locationListener.onLastLocationReceived(locationResult.getLastLocation());
                }
            }
        };
        locationCallbackReference = new LocationCallbackReference(locationCallback);
    }

    private void createLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10 * 1000); // milliseconds
        locationRequest.setFastestInterval(5 * 1000); // milliseconds
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    public void setLocationListener(LocationListener locationListener) {
        this.locationListener = locationListener;
    }

    public void setLocationRequestListener(LocationRequestListener locationRequestListener) {
        this.locationRequestListener = locationRequestListener;
    }

    public void startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallbackReference,
                Looper.getMainLooper());
    }

    public void stopLocationUpdates() {
        if (fusedLocationClient != null) {
            fusedLocationClient.removeLocationUpdates(locationCallbackReference);
        }
    }

    public boolean isLocationPermissionGranted(Context context) {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isLocationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return locationManager != null && locationManager.isLocationEnabled();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int locationMode;
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(),
                        Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            String locationProviders = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return locationProviders != null && !locationProviders.isEmpty();
        }
    }

    public void requestLastLocation() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(location -> {
                    if (location != null && locationListener != null) {
                        locationListener.onLastLocationReceived(location);
                    }
                });
    }

    public void requestLocationPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
    }

    public void enableLocation(Activity activity) {
        if (locationManager != null &&
                locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (locationRequestListener != null) {
                locationRequestListener.onRequestLocationResult(true);
            }
        } else {
            SettingsClient locationSettingsClient = LocationServices.getSettingsClient(activity);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            LocationSettingsRequest locationSettingsRequest = builder.build();
            locationSettingsClient
                    .checkLocationSettings(locationSettingsRequest)
                    .addOnSuccessListener(locationSettingsResponse -> {
                        //  GPS is already enable, callback GPS status through listener
                        if (locationRequestListener != null) {
                            locationRequestListener.onRequestLocationResult(true);
                        }
                    })
                    .addOnFailureListener(e -> {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    // Show the dialog by calling startResolutionForResult(),
                                    // and check the result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(activity, MY_REQUEST_LOCATION_ENABLE);
                                } catch (IntentSender.SendIntentException sie) {
                                    sie.printStackTrace();
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                // Location settings are inadequate, and cannot be fixed here.
                                // Fix in Settings.
                        }
                    })
                    .addOnCanceledListener(() -> {
                        if (locationRequestListener != null) {
                            locationRequestListener.onRequestLocationResult(false);
                        }
                    });
        }
    }
}