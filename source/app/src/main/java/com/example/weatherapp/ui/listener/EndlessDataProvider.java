package com.example.weatherapp.ui.listener;

import java.util.List;

public interface EndlessDataProvider<Item> {

    List<Item> getMoreData();

    void getMoreDataAsync();

    void reset();
}