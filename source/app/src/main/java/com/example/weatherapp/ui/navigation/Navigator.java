package com.example.weatherapp.ui.navigation;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.weatherapp.R;
import com.example.weatherapp.ui.fragment.BaseFragment;

public class Navigator {
    private Navigator() {
        // no-implementation
    }

    public static void addFragmentOnTop(FragmentManager fragmentManager,
                                        BaseFragment fragment) {
        fragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, fragment.getName())
                .addToBackStack(fragment.getName())
                .commitAllowingStateLoss();
    }

    public static void replaceFragment(FragmentManager fragmentManager,
                                       BaseFragment fragment) {
        Fragment oldFragment = fragmentManager.findFragmentByTag(fragment.getName());
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (oldFragment != null) {
            transaction.remove(oldFragment);
            fragmentManager.popBackStackImmediate();
        }
        transaction.replace(R.id.container, fragment, fragment.getName());
        transaction.commitNow();
    }
}
