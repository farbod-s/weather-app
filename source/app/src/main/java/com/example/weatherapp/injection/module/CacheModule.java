package com.example.weatherapp.injection.module;

import android.app.Application;

import com.example.cache.WeatherCacheImp;
import com.example.cache.db.AppDatabase;
import com.example.data.repository.WeatherCache;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class CacheModule {

    @Provides
    public static AppDatabase provideDatabase(Application application) {
        return AppDatabase.getInstance(application.getApplicationContext());
    }

    @Binds
    public abstract WeatherCache bindWeatherCache(WeatherCacheImp weatherCacheImp);
}
