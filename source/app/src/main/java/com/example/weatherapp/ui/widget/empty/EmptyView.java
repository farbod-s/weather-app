package com.example.weatherapp.ui.widget.empty;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.example.weatherapp.databinding.ViewEmptyBinding;

public class EmptyView extends RelativeLayout {
    private EmptyListener listener;

    public EmptyView(Context context) {
        super(context);
        init();
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        ViewEmptyBinding binding = ViewEmptyBinding.inflate(LayoutInflater.from(getContext()), this);
        binding.buttonCheckAgain.setOnClickListener(view -> {
            if (listener != null) {
                listener.onCheckAgainClicked();
            }
        });
    }

    public void setListener(EmptyListener listener) {
        this.listener = listener;
    }
}
