package com.example.weatherapp.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.example.presentation.viewmodel.ViewModelFactory;
import com.example.weatherapp.ui.MainActivity;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public abstract class BaseFragment extends Fragment {
    @Inject
    ViewModelFactory viewModelFactory;

    protected MainActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        deserializeArguments();

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);

        this.activity = (MainActivity) context;
    }

    @Override
    public void onDetach() {
        activity = null;
        super.onDetach();
    }

    protected <VM extends ViewModel> VM getViewModel(Class<VM> modelClass) {
        return ViewModelProviders.of(this, viewModelFactory).get(modelClass);
    }

    protected void deserializeArguments() {
        // no-implementation
    }

    public abstract String getName();

    protected abstract View getActionBarView();
}
