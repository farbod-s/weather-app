package com.example.weatherapp.location;

public interface LocationRequestListener {
    void onRequestLocationResult(boolean locationEnabled);
}