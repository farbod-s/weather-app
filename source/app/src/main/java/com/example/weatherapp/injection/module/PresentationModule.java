package com.example.weatherapp.injection.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.presentation.viewmodel.FindWeatherViewModel;
import com.example.presentation.viewmodel.GetWeatherViewModel;
import com.example.presentation.viewmodel.ViewModelFactory;
import com.example.weatherapp.injection.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class PresentationModule {
    @Binds
    @IntoMap
    @ViewModelKey(FindWeatherViewModel.class)
    public abstract ViewModel bindFindWeatherViewModel(FindWeatherViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(GetWeatherViewModel.class)
    public abstract ViewModel bindGetWeatherViewModel(GetWeatherViewModel viewModel);

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
