package com.example.weatherapp.injection.module;

import com.example.data.WeatherDataRepository;
import com.example.data.executor.JobExecutor;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.repository.WeatherRepository;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DataModule {

    @Binds
    public abstract WeatherRepository bindWeatherRepository(WeatherDataRepository weatherDataRepository);

    @Binds
    public abstract ThreadExecutor bindThreadExecutor(JobExecutor jobExecutor);
}
