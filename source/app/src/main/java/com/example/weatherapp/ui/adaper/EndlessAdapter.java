package com.example.weatherapp.ui.adapter;

import android.os.AsyncTask;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherapp.MyApplication;
import com.example.weatherapp.ui.listener.EndlessDataProvider;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class EndlessAdapter<Item, Holder extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<Holder> {
    private static final int DEFAULT_VIEW_TYPE = -10;
    private static final int PENDING_TYPE = -12;
    private static final int FIRST_PENDING_TYPE = -13;

    private AtomicBoolean keepAppending = new AtomicBoolean(true);
    private Boolean callInBackground;

    // Cache list for runInBackground
    protected List<Item> data;

    // cached data
    private List<Item> cache;

    private EndlessDataProvider<Item> dataProvider;

    public EndlessAdapter(EndlessDataProvider<Item> dataProvider) {
        this(dataProvider, false);
    }

    public EndlessAdapter(EndlessDataProvider<Item> dataProvider, boolean runInBackground) {
        this.dataProvider = dataProvider;
        this.callInBackground = runInBackground;
        this.data = new ArrayList<>();
    }

    @Override
    public final Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == PENDING_TYPE) {
            return getPendingViewHolder(parent);
        } else if (viewType == FIRST_PENDING_TYPE) {
            return getFirstPendingViewHolder(parent);
        }
        return onCreateItemHolder(parent, viewType);
    }

    @Override
    public final void onBindViewHolder(@NonNull Holder holder, int position) {
        if (position == data.size()) {
            if (callInBackground) {
                fetchDataInBackground();
            } else {
                setKeepOnAppending(true);
                MyApplication.runOnUIThread(() -> {
                    try {
                        dataProvider.getMoreDataAsync();
                    } catch (Exception e) {
                        setKeepOnAppending(true);
                    }
                });
            }
            return;
        }
        onBindItemHolder(holder, data.get(position));
    }

    @Override
    public final int getItemCount() {
        if (keepAppending.get()) {
            // 1 extra for pending
            return data.size() + 1;
        } else {
            return data.size();
        }
    }

    @Override
    public final int getItemViewType(int position) {
        // last position is pendingView
        if (position == data.size()) {
            if (data.size() == 0) {
                return FIRST_PENDING_TYPE;
            } else {
                return PENDING_TYPE;
            }
        }

        return getViewType(data.get(position));
    }

    public void reset() {
        data.clear();
        dataProvider.reset();
        resumeAppending();
        notifyDataSetChanged();
    }

    public void appendData(List<Item> data) {
        if (data == null || data.size() == 0) {
            stopAppending();
            return;
        }
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    protected boolean cacheInBackground() {
        cache = dataProvider.getMoreData();
        return (cache != null && cache.size() != 0);
    }

    protected void appendCachedData() {
        if (cache != null && cache.size() != 0) {
            data.addAll(cache);
        }
    }

    public void resumeAppending() {
        setKeepOnAppending(true);
    }

    public void stopAppending() {
        setKeepOnAppending(false);
    }

    public int getDataCount() {
        return data.size();
    }

    private void setKeepOnAppending(boolean newValue) {
        boolean same = (newValue == keepAppending.get());
        keepAppending.set(newValue);
        if (!same) {
            notifyDataSetChanged();
        }
    }

    private void fetchDataInBackground() {
        AppendTask task = new AppendTask(this);
        task.execute();
    }

    public int getViewType(Item item) {
        return DEFAULT_VIEW_TYPE;
    }

    public abstract void onBindItemHolder(Holder holder, Item item);

    public abstract Holder getPendingViewHolder(ViewGroup parent);

    public abstract Holder getFirstPendingViewHolder(ViewGroup parent);

    public abstract Holder onCreateItemHolder(ViewGroup parent, int viewType);

    private static class AppendTask extends AsyncTask<Void, Void, Exception> {
        private WeakReference<EndlessAdapter> adapter;
        private boolean keepAppending = false;

        public AppendTask(EndlessAdapter adapter) {
            this.adapter = new WeakReference<>(adapter);
        }

        @Override
        protected Exception doInBackground(Void... params) {
            Exception result = null;

            try {
                keepAppending = adapter.get().cacheInBackground();
            } catch (Exception e) {
                result = e;
            }

            return result;
        }

        @Override
        protected void onPostExecute(Exception e) {
            if (adapter != null && adapter.get() != null) {
                if (e == null) {
                    adapter.get().appendCachedData();
                    adapter.get().setKeepOnAppending(keepAppending);
                } else {
                    adapter.get().setKeepOnAppending(false);
                }
            }
        }
    }
}
