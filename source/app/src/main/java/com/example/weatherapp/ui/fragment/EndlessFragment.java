package com.example.weatherapp.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.presentation.data.Resource;
import com.example.presentation.data.ResourceState;
import com.example.weatherapp.R;
import com.example.weatherapp.connection.ConnectionHelper;
import com.example.weatherapp.databinding.FragmentListBinding;
import com.example.weatherapp.ui.adapter.EndlessAdapter;
import com.example.weatherapp.ui.listener.EndlessDataProvider;
import com.example.weatherapp.ui.listener.ItemClickListener;
import com.example.weatherapp.ui.viewholder.BaseViewHolder;
import com.example.weatherapp.ui.widget.empty.EmptyListener;
import com.example.weatherapp.ui.widget.error.ErrorListener;

import java.util.List;

public abstract class EndlessFragment<Item, Holder extends RecyclerView.ViewHolder> extends BaseFragment {
    private EndlessAdapter<Item, Holder> endlessAdapter;
    private EndlessDataProvider<Item> dataProvider;
    private FragmentListBinding binding;

    // Root View
    private View rootView;

    private EmptyListener emptyListener = this::resetList;

    private ErrorListener errorListener = this::resetList;

    private ItemClickListener<Item> clickListener = this::onItemClicked;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initDataProvider();
        initAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (rootView != null) {
            // reuse view if exist
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null) {
                parent.removeView(rootView);
            }
            return rootView;
        }

        binding = FragmentListBinding.inflate(inflater, container, false);

        setupActionBar();
        setupViewListeners();
        setupListView();

        rootView = binding.getRoot();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void setupActionBar() {
        binding.actionbar.setCustomView(getActionBarView(),
                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
    }

    private void initDataProvider() {
        dataProvider = getDataProvider();
    }

    private void initAdapter() {
        endlessAdapter = new EndlessAdapter<Item, Holder>(dataProvider) {
            @Override
            public void onBindItemHolder(Holder holder, Item item) {
                EndlessFragment.this.onBindItemHolder(holder, item);
            }

            @Override
            public Holder getPendingViewHolder(ViewGroup parent) {
                return EndlessFragment.this.getPendingViewHolder(parent);
            }

            @Override
            public Holder getFirstPendingViewHolder(ViewGroup parent) {
                return EndlessFragment.this.getFirstPendingViewHolder(parent);
            }

            @Override
            public int getViewType(Item item) {
                return EndlessFragment.this.getItemViewType(item);
            }

            @Override
            public Holder onCreateItemHolder(ViewGroup parent, int viewType) {
                return EndlessFragment.this.onCreateItemHolder(parent, viewType);
            }
        };
    }

    private void setupViewListeners() {
        binding.viewEmpty.setListener(emptyListener);
        binding.viewError.setListener(errorListener);
    }

    private void setupListView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                getContext(),
                RecyclerView.VERTICAL,
                false
        );
        binding.recyclerBrowse.setLayoutManager(layoutManager);
        binding.recyclerBrowse.setAdapter(endlessAdapter);
    }

    @SuppressWarnings("unchecked")
    private Holder getPendingViewHolder(ViewGroup parent) {
        return (Holder) new BaseViewHolder(LayoutInflater.from(getContext()).inflate(
                R.layout.view_loading,
                parent,
                false)
        );
    }

    @SuppressWarnings("unchecked")
    private Holder getFirstPendingViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_loading_first,
                parent,
                false);
        view.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                binding.recyclerBrowse.getMeasuredHeight()));
        return (Holder) new BaseViewHolder(view);
    }

    protected void resetList() {
        endlessAdapter.reset();
    }

    protected void stopAppending() {
        endlessAdapter.stopAppending();
    }

    protected void appendData(Resource<List<Item>> listResource) {
//        binding.setState(listResource.status);

        if (listResource.status == ResourceState.SUCCESS) {
            if (listResource.data != null && !listResource.data.isEmpty()) {
                endlessAdapter.appendData(listResource.data);
                binding.viewEmpty.setVisibility(View.GONE);
            } else {
                if (endlessAdapter.getDataCount() == 0) {
                    binding.viewEmpty.setVisibility(View.VISIBLE);
                }
            }
            // [openweathermap.org] does not support pagination! :(
            endlessAdapter.stopAppending();
        } else if (listResource.status == ResourceState.ERROR) {
            if (endlessAdapter.getDataCount() == 0) {
                if (!ConnectionHelper.isConnectedToInternet(activity)) {
                    binding.viewError.setMessage(R.string.label_no_internet_connection);
                } else {
                    binding.viewError.setMessage(R.string.label_error_result);
                }
                binding.viewError.setVisibility(View.VISIBLE);
            }
            endlessAdapter.stopAppending();
        } else {
            binding.viewError.setVisibility(View.GONE);
            binding.viewEmpty.setVisibility(View.GONE);
        }
    }

    protected ItemClickListener<Item> getClickListener() {
        return clickListener;
    }

    protected int getItemViewType(Item item) {
        return -10;
    }

    abstract protected Holder onCreateItemHolder(ViewGroup parent, int viewType);

    abstract protected void onBindItemHolder(Holder holder, Item item);

    abstract protected EndlessDataProvider<Item> getDataProvider();

    abstract protected void onItemClicked(Item item);
}
