package com.example.weatherapp.injection.component;

import android.app.Application;

import com.example.weatherapp.MyApplication;
import com.example.weatherapp.injection.module.ApplicationModule;
import com.example.weatherapp.injection.module.CacheModule;
import com.example.weatherapp.injection.module.DataModule;
import com.example.weatherapp.injection.module.DomainModule;
import com.example.weatherapp.injection.module.PresentationModule;
import com.example.weatherapp.injection.module.RemoteModule;
import com.example.weatherapp.injection.module.UiModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        AndroidSupportInjectionModule.class,
        CacheModule.class,
        DataModule.class,
        DomainModule.class,
        PresentationModule.class,
        RemoteModule.class,
        UiModule.class
})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        ApplicationComponent build();
    }

    void inject(MyApplication app);
}
