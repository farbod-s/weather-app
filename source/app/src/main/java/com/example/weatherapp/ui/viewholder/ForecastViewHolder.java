package com.example.weatherapp.ui.viewholder;

import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherapp.databinding.ItemForecastBinding;

public class ForecastViewHolder extends RecyclerView.ViewHolder {
    public ItemForecastBinding binding;

    public ForecastViewHolder(ItemForecastBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}