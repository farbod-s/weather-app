package com.example.weatherapp.ui.widget.error;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.example.weatherapp.databinding.ViewErrorBinding;

public class ErrorView extends RelativeLayout {

    private ViewErrorBinding binding;
    private ErrorListener listener;

    public ErrorView(Context context) {
        super(context);
        init();
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        binding = ViewErrorBinding.inflate(LayoutInflater.from(getContext()), this);
        binding.buttonTryAgain.setOnClickListener(view -> {
            if (listener != null) {
                listener.onTryAgainClicked();
            }
        });
    }

    public void setMessage(String message) {
        binding.textMessage.setText(message);
    }

    public void setMessage(int resId) {
        binding.textMessage.setText(resId);
    }

    public void setListener(ErrorListener listener) {
        this.listener = listener;
    }
}

