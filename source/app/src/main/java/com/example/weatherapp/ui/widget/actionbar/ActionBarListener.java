package com.example.weatherapp.ui.widget.actionbar;

import android.view.View;

public interface ActionBarListener {
    void onActionClicked(View view);
}
