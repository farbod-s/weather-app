package com.example.weatherapp.ui.adapter;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.squareup.picasso.Picasso;

public class BindingAdapters {

    @BindingAdapter(value = {"imageUrl", "placeholder"}, requireAll = false)
    public static void loadImage(ImageView view, String imageUrl, int placeholder) {
        if (placeholder > 0) {
            Picasso.get()
                    .load(imageUrl)
                    .placeholder(placeholder)
                    .into(view);
        } else {
            Picasso.get()
                    .load(imageUrl)
                    .into(view);
        }
    }
}
