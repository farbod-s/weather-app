package com.example.weatherapp.ui.viewholder;

import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherapp.databinding.ItemCityBinding;

public class CityViewHolder extends RecyclerView.ViewHolder {
    public ItemCityBinding binding;

    public CityViewHolder(ItemCityBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
