package com.example.weatherapp.location;

import android.location.Location;

public interface LocationListener {
    void onLastLocationReceived(Location location);
}
