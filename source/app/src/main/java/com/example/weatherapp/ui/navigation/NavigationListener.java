package com.example.weatherapp.ui.navigation;

public interface NavigationListener {
    void onNavigationChanged();
}
