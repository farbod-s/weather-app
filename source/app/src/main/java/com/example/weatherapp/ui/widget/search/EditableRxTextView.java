package com.example.weatherapp.ui.widget.search;

import android.widget.TextView;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;

import com.jakewharton.rxbinding3.InitialValueObservable;


public final class EditableRxTextView {
    @CheckResult
    @NonNull
    public static InitialValueObservable<CharSequence> textChanges(@NonNull TextView view) {
        return new EditableTextViewTextObservable(view);
    }
}
