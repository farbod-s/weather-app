package com.example.weatherapp.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.presentation.model.CurrentWeather;
import com.example.weatherapp.databinding.ItemForecastBinding;
import com.example.weatherapp.ui.viewholder.ForecastViewHolder;

import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastViewHolder> {
    private List<CurrentWeather> weatherList;

    public ForecastAdapter(List<CurrentWeather> weatherList) {
        this.weatherList = weatherList;
    }

    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemForecastBinding binding =
                ItemForecastBinding.inflate(
                        LayoutInflater.from(parent.getContext()),
                        parent,
                        false
                );
        return new ForecastViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder holder, int position) {
        holder.binding.setWeather(weatherList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }
}
