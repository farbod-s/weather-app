package com.example.weatherapp.ui.widget.error;

public interface ErrorListener {
    void onTryAgainClicked();
}
