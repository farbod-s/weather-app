package com.example.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForecastWeatherModel {
    @SerializedName("cod")
    @Expose
    public String code;

    @SerializedName("message")
    @Expose
    public int message;

    @SerializedName("cnt")
    @Expose
    public int count;

    @SerializedName("list")
    @Expose
    public List<WeatherModel> cityWeatherList;

    @SerializedName("city")
    @Expose
    public CityModel location;
}
