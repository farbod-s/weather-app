package com.example.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherResponse {
    @SerializedName("cod")
    @Expose
    public String code;

    @SerializedName("list")
    @Expose
    public List<WeatherModel> weatherList;

    @SerializedName("city")
    @Expose
    public CityModel location;
}
