package com.example.remote.service;

import com.example.remote.model.WeatherResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {
    @GET("forecast")
    Single<WeatherResponse> getWeatherByLocation(@Query("lat") String latitude,
                                                 @Query("lon") String longitude);

    @GET("forecast")
    Single<WeatherResponse> getWeatherByName(@Query("q") String cityName);

    @GET("find")
    Single<WeatherResponse> findWeather(@Query("q") String cityName,
                                        @Query("type") String queryType);
}