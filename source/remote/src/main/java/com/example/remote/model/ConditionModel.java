package com.example.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConditionModel {
    @SerializedName("temp")
    @Expose
    public double temperature;

    @SerializedName("temp_min")
    @Expose
    public double minTemperature;

    @SerializedName("temp_max")
    @Expose
    public double maxTemperature;

    @SerializedName("pressure")
    @Expose
    public double pressure;

    @SerializedName("humidity")
    @Expose
    public double humidity;
}
