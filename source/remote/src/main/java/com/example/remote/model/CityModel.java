package com.example.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityModel {
    @SerializedName("id")
    @Expose
    public long cityId;

    @SerializedName("name")
    @Expose
    public String cityName;

    @SerializedName("country")
    @Expose
    public String countryName;

    @SerializedName("timezone")
    @Expose
    public int timezone;
}
