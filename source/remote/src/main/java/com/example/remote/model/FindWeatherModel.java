package com.example.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FindWeatherModel {
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("cod")
    @Expose
    public String code;

    @SerializedName("count")
    @Expose
    public int count;

    @SerializedName("list")
    @Expose
    public List<WeatherModel> cityWeatherList;
}
