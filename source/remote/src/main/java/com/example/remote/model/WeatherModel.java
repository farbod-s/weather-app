package com.example.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherModel {
    @SerializedName("id")
    @Expose
    public int cityId;

    @SerializedName("name")
    @Expose
    public String cityName;

    @SerializedName("dt")
    @Expose
    public long datetimeTimestamp;

    @SerializedName("dt_txt")
    @Expose
    public String datetimeString;

    @SerializedName("main")
    @Expose
    public ConditionModel condition;

    @SerializedName("sys")
    @Expose
    public CityModel location;

    @SerializedName("weather")
    @Expose
    public List<DescriptionModel> weatherList;
}
