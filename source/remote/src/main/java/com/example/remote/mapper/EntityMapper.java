package com.example.remote.mapper;

interface EntityMapper<Model, Entity> {
    Entity mapFromRemote(Model type);
}