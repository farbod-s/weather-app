package com.example.remote.mapper;

import com.example.domain.model.Weather;
import com.example.remote.model.DescriptionModel;
import com.example.remote.model.WeatherModel;

import javax.inject.Inject;

public class WeatherEntityMapper implements EntityMapper<WeatherModel, Weather> {

    @Inject
    public WeatherEntityMapper() {
        // no-implementation
    }

    @Override
    public Weather mapFromRemote(WeatherModel type) {
        Weather weather = new Weather();
        weather.datetimeString = type.datetimeString;
        weather.datetimeTimestamp = type.datetimeTimestamp;
        DescriptionModel descriptionModel = type.weatherList != null && type.weatherList.size() > 0
                ? type.weatherList.get(0) : null;
        weather.description = descriptionModel != null ? descriptionModel.description : "-";
        weather.icon = descriptionModel != null ? descriptionModel.icon : "-";
        weather.title = descriptionModel != null ? descriptionModel.title : "-";
        weather.humidity = type.condition.humidity;
        weather.maxTemperature = type.condition.maxTemperature;
        weather.minTemperature = type.condition.minTemperature;
        weather.pressure = type.condition.pressure;
        weather.temperature = type.condition.temperature;

        return weather;
    }
}
