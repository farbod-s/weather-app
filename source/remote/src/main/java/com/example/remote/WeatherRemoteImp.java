package com.example.remote;

import com.example.data.repository.WeatherRemote;
import com.example.domain.model.City;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;
import com.example.remote.mapper.WeatherEntityMapper;
import com.example.remote.model.WeatherModel;
import com.example.remote.service.WeatherService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class WeatherRemoteImp implements WeatherRemote {

    private WeatherService remoteService;
    private WeatherEntityMapper entityMapper;

    @Inject
    public WeatherRemoteImp(WeatherService remoteService,
                            WeatherEntityMapper entityMapper) {
        this.remoteService = remoteService;
        this.entityMapper = entityMapper;
    }

    @Override
    public Single<WeatherForecast> getWeather(WeatherFilter filter) {
        if (filter.location != null) {
            return remoteService.getWeatherByLocation(filter.location.latitude,
                    filter.location.longitude)
                    .map(weatherResponse -> {
                        WeatherForecast weatherForecast = new WeatherForecast();
                        weatherForecast.weatherList = new ArrayList<>();
                        for (WeatherModel weatherModel : weatherResponse.weatherList) {
                            weatherForecast.weatherList.add(entityMapper.mapFromRemote(weatherModel));
                        }
                        weatherForecast.city = new City(weatherResponse.location.cityId,
                                weatherResponse.location.cityName,
                                weatherResponse.location.countryName,
                                weatherResponse.location.timezone);
                        return weatherForecast;
                    });
        } else {
            return remoteService.getWeatherByName(filter.cityName)
                    .map(weatherResponse -> {
                        WeatherForecast weatherForecast = new WeatherForecast();
                        weatherForecast.weatherList = new ArrayList<>();
                        for (WeatherModel weatherModel : weatherResponse.weatherList) {
                            weatherForecast.weatherList.add(entityMapper.mapFromRemote(weatherModel));
                        }
                        weatherForecast.city = new City(weatherResponse.location.cityId,
                                weatherResponse.location.cityName,
                                weatherResponse.location.countryName,
                                weatherResponse.location.timezone);
                        return weatherForecast;
                    });
        }
    }

    @Override
    public Flowable<List<WeatherForecast>> findWeather(WeatherFilter filter) {
        return remoteService.findWeather(filter.cityName, "like")
                .map(weatherResponse -> {
                    List<WeatherForecast> list = new ArrayList<>();
                    for (WeatherModel weatherModel : weatherResponse.weatherList) {
                        WeatherForecast weatherForecast = new WeatherForecast();
                        weatherForecast.weatherList = new ArrayList<>();
                        weatherForecast.weatherList.add(entityMapper.mapFromRemote(weatherModel));
                        weatherForecast.city = new City(weatherModel.cityId,
                                weatherModel.cityName,
                                weatherModel.location != null ? weatherModel.location.countryName : "-",
                                weatherModel.location != null ? weatherModel.location.timezone : 0);
                        list.add(weatherForecast);
                    }
                    return list;
                }).toFlowable();
    }
}