package com.example.domain.repository;

import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface WeatherRepository {
    Completable clearWeather();

    Completable saveWeather(WeatherForecast weather, Location location);

    Single<WeatherForecast> getWeather(WeatherFilter filter);

    Flowable<List<WeatherForecast>> findWeather(WeatherFilter filter);
}