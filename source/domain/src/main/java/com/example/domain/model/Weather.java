package com.example.domain.model;

public final class Weather {
    public long id;
    public String title;
    public String description;
    public String icon;
    public String datetimeString;
    public long datetimeTimestamp;
    public double temperature;
    public double minTemperature;
    public double maxTemperature;
    public double humidity;
    public double pressure;
}
