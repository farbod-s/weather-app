package com.example.domain.usecase.base;

import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Default [SingleObserver] base class to define
 */
public class SingleUseCase<T> implements SingleObserver<T> {

    @Override
    public void onSubscribe(@NonNull Disposable d) {
        // no-implementation
    }

    @Override
    public void onSuccess(@NonNull T t) {
        // no-implementation
    }

    @Override
    public void onError(@NonNull Throwable e) {
        // no-implementation
    }
}
