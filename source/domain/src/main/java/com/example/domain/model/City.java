package com.example.domain.model;

public class City {
    public long id;
    public String name;
    public String country;
    public int timezone;

    public City(long id,
                String name,
                String country,
                int timezone) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.timezone = timezone;
    }
}
