package com.example.domain.usecase;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.model.Location;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;
import com.example.domain.repository.WeatherRepository;
import com.example.domain.usecase.base.FlowableUseCase;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class GetWeather extends FlowableUseCase<WeatherForecast, WeatherFilter> {
    private WeatherRepository weatherRepository;

    @Inject
    public GetWeather(WeatherRepository weatherRepository,
                      ThreadExecutor threadExecutor,
                      PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);

        this.weatherRepository = weatherRepository;
    }

    @Override
    protected Flowable<WeatherForecast> buildUseCaseObservable(WeatherFilter filter) {
        return weatherRepository.getWeather(filter).toFlowable();
    }
}
