package com.example.domain.usecase;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;
import com.example.domain.model.WeatherFilter;
import com.example.domain.model.WeatherForecast;
import com.example.domain.repository.WeatherRepository;
import com.example.domain.usecase.base.FlowableUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class FindWeather extends FlowableUseCase<List<WeatherForecast>, WeatherFilter> {
    private WeatherRepository weatherRepository;

    @Inject
    public FindWeather(WeatherRepository weatherRepository,
                       ThreadExecutor threadExecutor,
                       PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);

        this.weatherRepository = weatherRepository;
    }

    @Override
    protected Flowable<List<WeatherForecast>> buildUseCaseObservable(WeatherFilter filter) {
        return weatherRepository.findWeather(filter);
    }
}