package com.example.domain.model;

import java.util.List;

public final class WeatherForecast {
    public City city;
    public List<Weather> weatherList;
}
