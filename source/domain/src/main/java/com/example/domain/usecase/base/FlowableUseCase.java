package com.example.domain.usecase.base;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Abstract class for a UseCase that returns an instance of a [Flowable].
 */
public abstract class FlowableUseCase<Output, Params> {
    private ThreadExecutor threadExecutor;
    private PostExecutionThread postExecutionThread;
    private CompositeDisposable disposables;

    public FlowableUseCase(ThreadExecutor threadExecutor,
                           PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
        this.disposables = new CompositeDisposable();
    }

    /**
     * Builds a [Flowable] which will be used when the current [FlowableUseCase] is executed.
     */
    protected abstract Flowable<Output> buildUseCaseObservable(Params params);

    /**
     * Executes the current use case.
     */
    public void execute(DisposableSubscriber<Output> observer, Params params) {
        Flowable<Output> observable = buildUseCaseObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());

        addDisposable(observable.subscribeWith(observer));
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    private void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }
}
