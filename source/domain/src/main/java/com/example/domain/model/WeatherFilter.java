package com.example.domain.model;

public final class WeatherFilter {
    public Location location;
    public String cityName;

    public WeatherFilter(Location location, String cityName) {
        this.location = location;
        this.cityName = cityName;
    }
}
