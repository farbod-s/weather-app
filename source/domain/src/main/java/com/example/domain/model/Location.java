package com.example.domain.model;

public final class Location {
    public String latitude;
    public String longitude;

    public Location(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public boolean isEmpty() {
        return latitude == null || latitude.isEmpty() || longitude == null || longitude.isEmpty();
    }

    public String[] split() {
        return new String[]{this.latitude, this.longitude};
    }

    public static Location fromString(String location) {
        if (location == null || location.isEmpty() || location.split(",").length != 2) {
            return null;
        }
        String[] latlng = location.split(",");
        return new Location(latlng[0], latlng[1]);
    }

    @Override
    public String toString() {
        return latitude + "," + longitude;
    }
}
