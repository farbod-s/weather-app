package com.example.domain.usecase.base;

import com.example.domain.executor.PostExecutionThread;
import com.example.domain.executor.ThreadExecutor;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Abstract class for a UseCase that returns an instance of a [Completable].
 */
abstract class CompletableUseCase<Params> {
    private ThreadExecutor threadExecutor;
    private PostExecutionThread postExecutionThread;
    private Disposable subscription;

    public CompletableUseCase(ThreadExecutor threadExecutor,
                              PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
        this.subscription = new Disposable() {
            @Override
            public void dispose() {
                // no-implementation
            }

            @Override
            public boolean isDisposed() {
                return false;
            }
        };
    }

    /**
     * Builds a [Completable] which will be used when the current [CompletableUseCase] is executed.
     */
    protected abstract Completable buildUseCaseObservable(Params params);

    /**
     * Executes the current use case.
     */
    public Completable execute(Params params) {
        return buildUseCaseObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
    }

    /**
     * Unsubscribes from current [Disposable].
     */
    public void unsubscribe() {
        if (!subscription.isDisposed()) {
            subscription.dispose();
        }
    }
}
